package main

import (
	"database/sql"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	_ "github.com/go-sql-driver/mysql"

	"todo-app-server/handlers"
	"todo-app-server/repository"
	"todo-app-server/services"
)

func getDBPool() (db *sql.DB, err error) {
	cnnString := os.ExpandEnv("${DB_USERNAME}:${DB_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_NAME}?parseTime=true")
	return sql.Open("mysql", cnnString)
}

// env dosyasını aktarır
func initEnv() error {
	err := godotenv.Load(".env")
	return err
}

// veritabanı tablolarını oluşturur
func initDB() error {
	db, err := getDBPool()
	if err != nil {
		return err
	}
	res, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS todo (
			Id int(11) NOT NULL AUTO_INCREMENT,
			Content text NOT NULL,
			Timestamp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			Completed char(1) NOT NULL DEFAULT '0',
			PRIMARY KEY (Id)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
	`)
	if err != nil {
		return err
	}
	_, err = res.RowsAffected()
	if err != nil {
		return err
	}
	return nil
}

func main() {
	// ENV dosyasını varsa işle
	// ENV dosyası belirtilmemişse env paslıyordur
	initEnv()

	// Veritabanı yükle
	err := initDB()
	if err != nil {
		panic(err.Error())
	}

	// Sunumu ayarlar
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	// e.Use(middleware.CORS())

	dbPool, _ := getDBPool()
	todoRepository := repository.NewTodoRepository(dbPool)
	todoService := services.NewTodoService(todoRepository)
	todoHandler := handlers.NewTodoHandler(todoService)

	e.GET("/todos", todoHandler.GetAllTodos)
	e.POST("/todos", todoHandler.AddTodo)
	e.PUT("/todos/:id/completed", todoHandler.SetCompleted)
	e.PUT("/todos/:id/uncompleted", todoHandler.SetUnCompleted)
	e.DELETE("/todos/:id", todoHandler.DeleteTodo)

	// bu portda başlat
	e.Logger.Fatal(e.Start(os.ExpandEnv(":${PORT}")))
}
