package models

import "time"

type Todo struct {
	Id        int       `json:"id"`
	Content   string    `json:"content"`
	Completed bool      `json:"completed"`
	Timestamp time.Time `json:"timestamp"`
}
