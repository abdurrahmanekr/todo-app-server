FROM golang

RUN mkdir -p /app 
WORKDIR /app
ADD . /app

RUN go mod download
RUN go build ./server.go

CMD ["./server"]