package repository

import (
	"context"
	"database/sql"
	"todo-app-server/models"
)

type Repository interface {
	GetAllTodos() interface{}
	AddTodo(content string) interface{}
	SetCompleted(id int) interface{}
	SetUnCompleted(id int) interface{}
	DeleteTodo(id int)
}

type TodoRepository struct {
	collection *sql.DB
}

func NewTodoRepository(collection *sql.DB) Repository {
	return TodoRepository{collection: collection}
}

func (r TodoRepository) GetAllTodos() interface{} {
	var todos = make([]models.Todo, 0)

	results, _ := r.collection.QueryContext(context.Background(), "SELECT Id, Content, Completed, Timestamp FROM todo ORDER BY Completed, Timestamp DESC")
	for results.Next() {
		var todo models.Todo
		// satır satır oku ve todos'a at
		results.Scan(&todo.Id, &todo.Content, &todo.Completed, &todo.Timestamp)
		todos = append(todos, todo)
	}

	return todos
}

func (r TodoRepository) AddTodo(content string) interface{} {
	todo := models.Todo{
		Content: content,
	}

	stmt, _ := r.collection.PrepareContext(context.Background(), "INSERT INTO todo (Content) values (?)")
	stmt.Exec(&todo.Content)

	return todo
}

func (r TodoRepository) SetCompleted(id int) interface{} {
	stmt, _ := r.collection.PrepareContext(context.Background(), "UPDATE todo SET Completed = '1' WHERE Id = ?")
	stmt.Exec(&id)
	return nil
}

func (r TodoRepository) SetUnCompleted(id int) interface{} {
	stmt, _ := r.collection.PrepareContext(context.Background(), "UPDATE todo SET Completed = '0' WHERE Id = ?")
	stmt.Exec(&id)
	return nil
}

func (r TodoRepository) DeleteTodo(id int) {
	stmt, _ := r.collection.PrepareContext(context.Background(), "DELETE FROM todo WHERE Id = ?")
	stmt.Exec(&id)
}
