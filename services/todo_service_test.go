package services

import (
	"testing"
	"todo-app-server/models"
	"todo-app-server/repository"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func Test_ShouldGetAllTodos(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todoList []models.Todo
	todoList = append(todoList, models.Todo{
		Id:        1,
		Content:   "Buy some milk",
		Completed: false,
	})

	repository := repository.NewMockRepository(controller)
	repository.EXPECT().GetAllTodos().Return(todoList).Times(1)
	service := TodoService{repository}

	all := service.GetAllTodos().([]models.Todo)
	assert.NotNil(t, all)
	assert.NotEmpty(t, all)
	assert.Equal(t, 1, len(all))
	assert.Equal(t, 1, all[0].Id)
	assert.Equal(t, "Buy some milk", all[0].Content)
	assert.False(t, all[0].Completed)
}

func Test_ShouldAddTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repository := repository.NewMockRepository(controller)
	repository.EXPECT().AddTodo("Buy some milk").Return(models.Todo{
		Id:        1,
		Content:   "Buy some milk",
		Completed: false,
	}).Times(1)

	service := NewTodoService(repository)
	created := service.AddTodo("Buy some milk")

	assert.NotNil(t, created)
	todo := created.(models.Todo)
	assert.Equal(t, 1, todo.Id)
	assert.Equal(t, "Buy some milk", todo.Content)
	assert.False(t, todo.Completed)
}

func Test_ShouldDeleteTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repository := repository.NewMockRepository(controller)
	repository.EXPECT().DeleteTodo(1).Times(1)

	service := TodoService{repository}
	service.DeleteTodo(1)
}
