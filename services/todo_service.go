package services

import (
	"todo-app-server/repository"
)

type Service interface {
	GetAllTodos() interface{}
	AddTodo(content string) interface{}
	SetCompleted(id int) interface{}
	SetUnCompleted(id int) interface{}
	DeleteTodo(id int)
}

type TodoService struct {
	repository repository.Repository
}

func NewTodoService(repository repository.Repository) Service {
	return TodoService{repository}
}

func (s TodoService) GetAllTodos() interface{} {
	return s.repository.GetAllTodos()
}

func (s TodoService) AddTodo(content string) interface{} {
	return s.repository.AddTodo(content)
}

func (s TodoService) SetCompleted(id int) interface{} {
	return s.repository.SetCompleted(id)
}

func (s TodoService) SetUnCompleted(id int) interface{} {
	return s.repository.SetUnCompleted(id)
}

func (s TodoService) DeleteTodo(id int) {
	s.repository.DeleteTodo(id)
}
