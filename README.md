# ToDo Application with Golang
Bu uygulama golang ile yazılmış bir todo örneğidir. Proje aynı zamanda react ile bir arayüze sahiptir. [Arayüz bağlantısı:](https://gitlab.com/abdurrahmanekr/todo-app)

Geçici olarak bu adresten yayınlanıyor: [http://157.230.121.51/](http://157.230.121.51/)

## Kullanılan teknolojiler

- Golang

## Özellikleri

ToDo eklemek/silmek/tamamlamak

## Kurmak için yapılanlar

Gereklilikler;

- Golang environment


`.env` dosyasının içerisine mysql bilgilerini girerek uygulamayı çalıştırabilirsiniz:

```
cp .env.template .env
```

Kurmak için:

```
go mod download
docker build -t abdurrahmanekr/todo-app-server .
docker run abdurrahmanekr/todo-app-server
```

## Deployment
Proje Docker Swarm çalışan bir sunucuya deploy ediliyor. Bu projedeki docker-compose.yml dosyası o sunucunun içinde ve aşağıdaki komutu çalıştırmak uygulamaların ayağa kalkması için yeterli:

```
docker stack deploy --compose-file docker-compose.yml todo-app
```