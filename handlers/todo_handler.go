package handlers

import (
	"net/http"
	"strconv"
	"todo-app-server/models"
	"todo-app-server/services"

	"github.com/labstack/echo/v4"
)

type TodoHandler struct {
	service services.Service
}

type Handler interface {
	GetAllTodos(c echo.Context) error
	AddTodo(c echo.Context) error
	SetCompleted(c echo.Context) error
	SetUnCompleted(c echo.Context) error
	DeleteTodo(c echo.Context) error
}

func NewTodoHandler(service services.Service) Handler {
	return TodoHandler{service}
}

func (handler TodoHandler) GetAllTodos(c echo.Context) error {
	return c.JSON(http.StatusOK, handler.service.GetAllTodos())
}

func (handler TodoHandler) AddTodo(c echo.Context) error {
	t := new(models.Todo)
	if err := c.Bind(t); err != nil {
		return c.JSON(http.StatusBadRequest, "")
	}

	return c.JSON(http.StatusCreated, handler.service.AddTodo(t.Content))
}

func (handler TodoHandler) SetCompleted(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))

	return c.JSON(http.StatusOK, handler.service.SetCompleted(id))
}

func (handler TodoHandler) SetUnCompleted(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))

	return c.JSON(http.StatusOK, handler.service.SetUnCompleted(id))
}

func (handler TodoHandler) DeleteTodo(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	handler.service.DeleteTodo(id)

	return c.JSON(http.StatusOK, "")
}
