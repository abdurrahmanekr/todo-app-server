package handlers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"todo-app-server/models"
	"todo-app-server/services"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

type CreateTodoRequest struct {
	Content string `json:"content"`
}

func Test_ShouldReturnEmptyTodoList(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	todos := make([]models.Todo, 0)
	service := services.NewMockService(controller)
	service.EXPECT().GetAllTodos().Return(todos).Times(1)

	handler := TodoHandler{service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	expected := `[]
`

	if assert.NoError(t, handler.GetAllTodos(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expected, rec.Body.String())
	}
}

func Test_ShouldGetAllTodos(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todos []models.Todo
	todos = append(todos, models.Todo{
		Id:        1,
		Content:   "Buy some milk",
		Completed: false,
	})

	service := services.NewMockService(controller)
	service.EXPECT().GetAllTodos().Return(todos).Times(1)
	handler := TodoHandler{service}

	expected := `[{"id":1,"content":"Buy some milk","completed":false,"timestamp":"0001-01-01T00:00:00Z"}]
`

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.GetAllTodos(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expected, rec.Body.String())
	}
}

func Test_ShouldCreateTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	requestAsString := `{"content":"Buy some milk"}`
	expected := models.Todo{
		Id:        1,
		Content:   "Buy some milk",
		Completed: false,
	}
	expectedAsString := `{"id":1,"content":"Buy some milk","completed":false,"timestamp":"0001-01-01T00:00:00Z"}
`

	service := services.NewMockService(controller)
	service.EXPECT().AddTodo("Buy some milk").Return(expected).Times(1)

	handler := TodoHandler{service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/todos", strings.NewReader(requestAsString))
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.AddTodo(ctx)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, expectedAsString, rec.Body.String())
	}
}

func Test_ShouldDeleteTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := services.NewMockService(controller)
	service.EXPECT().DeleteTodo(1)
	handler := TodoHandler{service}

	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/todos/1", nil)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)
	ctx.SetParamNames("id")
	ctx.SetParamValues("1")

	if assert.NoError(t, handler.DeleteTodo(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}
